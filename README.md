<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-15-2024 and will be end of life on 04-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Regex Operations](https://gitlab.com/itentialopensource/pre-built-automations/iap-regex-operations)

<!-- Update the below line with your artifact name -->
# Regex Test

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview
This Pre-Built Transformation enables users to execute a search for a match between a regular expression and a specified string. If there is a match between the regular expression and the string, it will return `true` Otherwise, the Transformation will return `false`.
<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->


## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`

## How to Install

To install the artifact:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the artifact. 
* The artifact can be installed from within App-Admin_Essential. Simply search for the name of your desired artifact and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the artifact:
1. Once the JST is installed as outlined in the How to Install section above, navigate to the workflow where you would like to use `test` and add a JSON Transformation task.
2. Inside the Transformation task, search for and select `test`.
3. Save your input and the task is ready to run inside of IAP.
<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
